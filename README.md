# Secrets Store

This is an abstraction package for a secrets store implementation. For now, it
only supports AWS Secrets Manager. In the long term, a support for Hashicorp
Vault will be added.

## Getting started

Import the secrets_store python package

```shell
import --index-url https://"TBD"/simple secrets_store==0.0.1
```

Call the function to retrieve the secret value

```shell
value = secrets_store.get_secret_value(secret_key=key_name)
```

## Development environment

Setting up Local Dev environment:

* Make virtual environment
* Testing by running:

    ```shell
    make all
    ```
