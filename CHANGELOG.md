# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Added

* Poetry
  ([!78](https://gitlab.com/CentOS/automotive/coffre/secrets-store/-/merge_requests/78))
* Renovate precommit
  ([!61](https://gitlab.com/CentOS/automotive/coffre/secrets-store/-/merge_requests/61))
* Renovate presets
  ([!60](https://gitlab.com/CentOS/automotive/coffre/secrets-store/-/merge_requests/60))
* Refactoring
  ([!59](https://gitlab.com/CentOS/automotive/coffre/secrets-store/-/merge_requests/59))
