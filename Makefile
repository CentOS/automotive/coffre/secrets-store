.PHONY: all depend precommit up down dependencies build publish unit functional

ifndef CI
  HOST := localhost
  PYPI_USERNAME := a
  PYPI_PASSWORD := a
  ARTIFACTS_STORE_URL := http://a:a@${HOST}:8080/
  ifneq (, $(shell which podman podman-compose))
    CONTAINER_COMPOSE_ENGINE := podman-compose
  else
    $(error ERROR)
  endif
endif

ifeq "$(ENV)" "CI"
  HOST := docker
  PYPI_USERNAME := a
  PYPI_PASSWORD := a
  CONTAINER_COMPOSE_ENGINE := docker-compose
  ARTIFACTS_STORE_URL := http://a:a@${HOST}:8080/
  RUN_IN_CONTAINER := docker run --rm -i -v ${PWD}/.:/code -w /code \
    --network host python:${PYTHONVER}
endif

ifeq "$(ENV)" "PROD"
  PYPI_USERNAME := gitlab-ci-token
  PYPI_PASSWORD := ${CI_JOB_TOKEN}
  ARTIFACTS_STORE_URL := ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
endif

up:
	cd tests/functional && ${CONTAINER_COMPOSE_ENGINE} up -d
	curl --head -X GET --retry 10 --retry-connrefused --retry-delay 1 \
		${ARTIFACTS_STORE_URL}

down:
	cd tests/functional && ${CONTAINER_COMPOSE_ENGINE} down

depend:
	curl -sSL https://install.python-poetry.org | python3 - --version 1.2.2
	poetry install --with "$a"

precommit:
	poetry run pre-commit run --all-files

build:
	poetry build

publish:
	poetry config repositories.gitlab ${ARTIFACTS_STORE_URL}
	poetry config http-basic.gitlab ${PYPI_USERNAME} ${PYPI_PASSWORD}
	poetry publish -vvv --repository gitlab

unit:
	poetry run pytest --cov=secrets_store --cov-report term-missing \
	--cov-fail-under=100 -v tests/unit

# The curl command (for unknown reasn) is needed for the test to work
functional:
	${RUN_IN_CONTAINER} curl http://localhost:8080 && pip3 install \
		--trusted-host ${HOST} \
		--index-url ${ARTIFACTS_STORE_URL}/simple secrets_store==0.0.1 && \
		poetry run pytest tests/functional

all: depend precommit unit build up publish functional down
