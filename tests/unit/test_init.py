from secrets_store import main


def test_init() -> bool:
    assert "get_secret_value" in dir(main)
