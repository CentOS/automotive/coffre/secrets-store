import boto3
import pytest
from moto import mock_secretsmanager

import secrets_store


@pytest.mark.parametrize("key,value", [("secret_key", "secret_value")])
@mock_secretsmanager
def test_main(key, value) -> bool:
    client = boto3.client(
        service_name="secretsmanager", region_name="eu-west-1"
    )
    client.create_secret(
        Name=key,
        SecretString=value,
    )
    assert secrets_store.get_secret_value(secret_key=key) == value
